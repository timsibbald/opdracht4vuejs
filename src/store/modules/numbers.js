const state = () => ({
    list: [],
    start:1,
    end:100
});

const getters = {
    getList(state) {
        return state.list;
    }
};

const mutations = {
    mutateList(state, data) {
       state.list.push(data)
    },
    emptyList(state) {
        state.list = []
    },
    mutateStart(state, data){
        state.start = data
    },
    mutateEnd(state, data){
        state.end = data
    }
};

const actions = {
    setList({commit,state}){
        commit('emptyList')

        for (let i = Number(state.start); i < Number(state.end) + 1; i++) {
            let isMultiplied5 = i % 5 == 0 && i > 5;
            let isMultiplied3 = i % 3 == 0 && i > 3;
            
            if (isMultiplied3 && isMultiplied5) {
                commit('mutateList', "CloudSuite")
            }
            if (isMultiplied3 && !isMultiplied5) {
                commit('mutateList', "Suite")
            }
            if (!isMultiplied3 && isMultiplied5) {
                commit('mutateList', "Cloud")
            }
            if (!isMultiplied3 && !isMultiplied5) {
                commit('mutateList', i)
            }
        }
    },
    setStart({commit}, data){
        commit('mutateStart', data)
    },
    setEnd({commit}, num){
        commit('mutateEnd', num)
    }

};

export default {
    state,
    getters,
    mutations,
    actions,
};